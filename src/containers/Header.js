import React, {Component} from 'react';

import Navbar from 'react-bootstrap/lib/Navbar';

class Header extends Component {
	render() {
		return(
      <div>
			<div className="main-header">
        <span href="" className="logo" >

          <span className="logo-mini"><b>A</b>LT</span>

          <span className="logo-lg"><b>Admin</b>LTE</span>
        </span>

        <Navbar fluid={false} className="navbar navbar-static-top" role="navigation">

          <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </a>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">

              <li className="dropdown user user-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <span className="hidden-xs"><i className="fa fa-user"></i> Alexander Pierce <i className="fa fa-caret-down"></i></span>
                </a>
                <ul className="dropdown-menu">

                  <li className="user-header">
                    
                    <p>
                      Alexander Pierce - Web Developer
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>

                  <li className="user-body">
                    <div className="col-xs-4 text-center">
                      <a href="">Followers</a>
                    </div>
                    <div className="col-xs-4 text-center">
                      <a href="">Sales</a>
                    </div>
                    <div className="col-xs-4 text-center">
                      <a href="">Friends</a>
                    </div>
                  </li>

                  <li className="user-footer">
                    <div className="pull-left">
                      <a href="" className="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div className="pull-right">
                      <a href="" className="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </Navbar>
      </div>

      <div className="main-sidebar">
        <section className="sidebar">
          <div className="user-panel">
            <div className="pull-left image">
              <img src="../../dist/img/user2-160x160.jpg" className="img-circle" />
            </div>
            <div className="pull-left info">
              <p>Alexander Pierce</p>
              <a href="#"><i className="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <form action="#" method="get" className="sidebar-form">
            <div className="input-group">
              <input type="text" name="q" className="form-control" placeholder="Search..." />
              <span className="input-group-btn">
                <button type="submit" name="search" id="search-btn" className="btn btn-flat"><i className="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <ul className="sidebar-menu">
            <li className="header">MAIN NAVIGATION</li>
            <li className="treeview">
              <a href="#">
                <i className="fa fa-dashboard"></i> <span>Dashboard</span> <i className="fa fa-angle-left pull-right"></i>
              </a>
              <ul className="treeview-menu">
                <li><a href="../../index.html"><i className="fa fa-circle-o"></i> Dashboard v1</a></li>
                <li><a href="../../index2.html"><i className="fa fa-circle-o"></i> Dashboard v2</a></li>
              </ul>
            </li>
            <li className="treeview">
              <a href="#">
                <i className="fa fa-files-o"></i>
                <span>Layout Options</span>
                <span className="label label-primary pull-right">4</span>
              </a>
              <ul className="treeview-menu">
                <li><a href="../layout/top-nav.html"><i className="fa fa-circle-o"></i> Top Navigation</a></li>
                <li><a href="../layout/boxed.html"><i className="fa fa-circle-o"></i> Boxed</a></li>
                <li><a href="../layout/fixed.html"><i className="fa fa-circle-o"></i> Fixed</a></li>
                <li><a href="../layout/collapsed-sidebar.html"><i className="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
              </ul>
            </li>
            <li>
              <a href="../widgets.html">
                <i className="fa fa-th"></i> <span>Widgets</span> <small className="label pull-right bg-green">Hot</small>
              </a>
            </li>
            <li className="treeview">
              <a href="#">
                <i className="fa fa-pie-chart"></i>
                <span>Charts</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>
              <ul className="treeview-menu">
                <li><a href="../charts/chartjs.html"><i className="fa fa-circle-o"></i> ChartJS</a></li>
                <li><a href="../charts/morris.html"><i className="fa fa-circle-o"></i> Morris</a></li>
                <li><a href="../charts/flot.html"><i className="fa fa-circle-o"></i> Flot</a></li>
                <li><a href="../charts/inline.html"><i className="fa fa-circle-o"></i> Inline charts</a></li>
              </ul>
            </li>
            <li className="treeview">
              <a href="#">
                <i className="fa fa-laptop"></i>
                <span>UI Elements</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>
              <ul className="treeview-menu">
                <li><a href="../UI/general.html"><i className="fa fa-circle-o"></i> General</a></li>
                <li><a href="../UI/icons.html"><i className="fa fa-circle-o"></i> Icons</a></li>
                <li><a href="../UI/buttons.html"><i className="fa fa-circle-o"></i> Buttons</a></li>
                <li><a href="../UI/sliders.html"><i className="fa fa-circle-o"></i> Sliders</a></li>
                <li><a href="../UI/timeline.html"><i className="fa fa-circle-o"></i> Timeline</a></li>
                <li><a href="../UI/modals.html"><i className="fa fa-circle-o"></i> Modals</a></li>
              </ul>
            </li>
            <li className="treeview">
              <a href="#">
                <i className="fa fa-edit"></i> <span>Forms</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>
              <ul className="treeview-menu">
                <li><a href="../forms/general.html"><i className="fa fa-circle-o"></i> General Elements</a></li>
                <li><a href="../forms/advanced.html"><i className="fa fa-circle-o"></i> Advanced Elements</a></li>
                <li><a href="../forms/editors.html"><i className="fa fa-circle-o"></i> Editors</a></li>
              </ul>
            </li>
            <li className="treeview">
              <a href="#">
                <i className="fa fa-table"></i> <span>Tables</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>
              <ul className="treeview-menu">
                <li><a href="../tables/simple.html"><i className="fa fa-circle-o"></i> Simple tables</a></li>
                <li><a href="../tables/data.html"><i className="fa fa-circle-o"></i> Data tables</a></li>
              </ul>
            </li>
            <li>
              <a href="../calendar.html">
                <i className="fa fa-calendar"></i> <span>Calendar</span>
                <small className="label pull-right bg-red">3</small>
              </a>
            </li>
            <li>
              <a href="../mailbox/mailbox.html">
                <i className="fa fa-envelope"></i> <span>Mailbox</span>
                <small className="label pull-right bg-yellow">12</small>
              </a>
            </li>
            <li className="treeview active">
              <a href="#">
                <i className="fa fa-folder"></i> <span>Examples</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>
              <ul className="treeview-menu">
                <li><a href="invoice.html"><i className="fa fa-circle-o"></i> Invoice</a></li>
                <li><a href="profile.html"><i className="fa fa-circle-o"></i> Profile</a></li>
                <li><a href="login.html"><i className="fa fa-circle-o"></i> Login</a></li>
                <li><a href="register.html"><i className="fa fa-circle-o"></i> Register</a></li>
                <li><a href="lockscreen.html"><i className="fa fa-circle-o"></i> Lockscreen</a></li>
                <li><a href="404.html"><i className="fa fa-circle-o"></i> 404 Error</a></li>
                <li><a href="500.html"><i className="fa fa-circle-o"></i> 500 Error</a></li>
                <li className="active"><a href="blank.html"><i className="fa fa-circle-o"></i> Blank Page</a></li>
              </ul>
            </li>
            <li className="treeview">
              <a href="#">
                <i className="fa fa-share"></i> <span>Multilevel</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>
              <ul className="treeview-menu">
                <li><a href="#"><i className="fa fa-circle-o"></i> Level One</a></li>
                <li>
                  <a href="#"><i className="fa fa-circle-o"></i> Level One <i className="fa fa-angle-left pull-right"></i></a>
                  <ul className="treeview-menu">
                    <li><a href="#"><i className="fa fa-circle-o"></i> Level Two</a></li>
                    <li>
                      <a href="#"><i className="fa fa-circle-o"></i> Level Two <i className="fa fa-angle-left pull-right"></i></a>
                      <ul className="treeview-menu">
                        <li><a href="#"><i className="fa fa-circle-o"></i> Level Three</a></li>
                        <li><a href="#"><i className="fa fa-circle-o"></i> Level Three</a></li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li><a href="#"><i className="fa fa-circle-o"></i> Level One</a></li>
              </ul>
            </li>
            <li><a href="../../documentation/index.html"><i className="fa fa-book"></i> <span>Documentation</span></a></li>
            <li className="header">LABELS</li>
            <li><a href="#"><i className="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i className="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i className="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
          </ul>
        </section>
      </div>
      </div>

			)
	}
}

export default Header
