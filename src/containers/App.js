import React, { Component } from 'react';
import Header from './Header';

class App extends Component {
  render() {
    return (
     <div id="wrapper skin-blue" className="skin-blue">
     <Header />
     </div>
     );
   }
 }

 export default App;
