import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';

import './styles/style.css';
import 'bootstrap/dist/css/bootstrap.css';
import './styles/AdminLTE.css';
import './styles/skin-blue.min.css';
import 'font-awesome/css/font-awesome.css';

ReactDOM.render(<App />,
	document.getElementById('root')
	);
